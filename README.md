**Edit a file, create a new file, and clone from Bitbucket in under 2 minutes**

## Решения домашней работы Java Collections

1) В Main статические функции, решающие задланные задачи

2) В Tester unit тесты, по одному на каждую задачу.

3) Для проверки нужно запустить Tester,
 в консоль записывается ход выполнения операций и конечные результаты каждой задачи


Now that you're more familiar with your Bitbucket repository, go ahead and add a new file locally. You can [push your change back to Bitbucket with SourceTree](https://confluence.atlassian.com/x/iqyBMg), or you can [add, commit,](https://confluence.atlassian.com/x/8QhODQ) and [push from the command line](https://confluence.atlassian.com/x/NQ0zDQ).