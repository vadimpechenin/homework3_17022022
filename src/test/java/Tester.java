import org.junit.Test;

public class Tester {
    @Test
    public void test1() {
        assert (Main.iteratorTask1().equals("Слон 6"));
    }

    @Test
    public void test2() {
        assert (Main.reversePolihNotation().equals("3 4 2 * 1 5 - / 2 * +"));
    }

    @Test
    public void test3() {
        assert (Main.mapForSymbolsFunction().equals("А: 2"));
    }
}
