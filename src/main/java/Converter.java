/*
Алгоритм взят с https://ru.wikipedia.org/wiki/Обратная_польская_запись
 */

import java.util.HashMap;
import java.util.Stack;

public class Converter {
    private String [] strings;
    //Словарь приоритетов
    HashMap<String, Integer> map =
            new HashMap<String, Integer>();

    public Converter(String [] strings){
        this.strings = strings;
        map.put("+", 1);
        map.put("-", 1);
        map.put("/", 2);
        map.put("*", 2);
        map.put("(", 0);
        map.put(")", 3);
        convert();
    }

    public String convert(){
        StringBuilder stringRPN = new StringBuilder();
        Stack<String> symbols = new Stack<String>();

        int k = 0;
        int iteration = 1;
        for (String i : strings){
            System.out.println("Шаг " + iteration);
            System.out.println("Читаем " + i);
            if (i.equals("+")||i.equals("-")||i.equals("*")||
                    i.equals("/")||i.equals("(")||i.equals(")")){
                if (symbols.size()>0){
                    if (!i.equals(")")){
                        if (!i.equals("(")) {
                            if (map.get(i).equals(map.get(symbols.peek()))) {
                                System.out.println("Добавляем к строке " + symbols.peek());
                                stringRPN.append(" ");
                                stringRPN.append(symbols.pop());
                                System.out.println("Добавляем к стэку " + i);
                                symbols.push(i);
                            } else {
                                symbols.push(i);
                            }
                        }else{
                            System.out.println("Добавляем на стек " + i);
                            symbols.push(i);
                        }
                    }else{
                        while(symbols.size()>0&&(!symbols.peek().equals("("))){
                            System.out.println("Добавляем к строке " + symbols.peek());
                            stringRPN.append(" ");
                            stringRPN.append(symbols.pop());
                        }
                        System.out.println("Убираем из стека " + symbols.peek());
                        symbols.pop();
                    }
                }else {
                    System.out.println("Добавляем к стэку " + i);
                    symbols.push(i);
                }
            }else{
                if (k==0) {
                    System.out.println("Добавляем к строке " + i);
                    stringRPN.append(i);
                    k++;
                }else{
                    System.out.println("Добавляем к строке " + i);
                    stringRPN.append(" ");
                    stringRPN.append(i);
                }
            }
            iteration++;
            System.out.println("Выход " + stringRPN.toString());
            System.out.println("Стэк " + symbols.toString());
        }
        while (symbols.size()>0){
            System.out.println("Добавляем к строке " + symbols.peek());
            stringRPN.append(" ");
            stringRPN.append(symbols.pop());
            System.out.println("Выход " + stringRPN.toString());
            System.out.println("Стэк " + symbols.toString());
        }
        return stringRPN.toString();
    }
}
