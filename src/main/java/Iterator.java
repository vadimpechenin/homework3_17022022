import java.util.ArrayList;
import java.util.Arrays;

public class Iterator {
    private int size = 0;
    private static int DEFAULT_CAPACITY = 10;
    private String[] iterator = new String[DEFAULT_CAPACITY];

    public Iterator() {
    }
    public void add(String value){
        if (size<DEFAULT_CAPACITY) {
            this.iterator[size] = value;
            size++;
        }else{
            DEFAULT_CAPACITY= DEFAULT_CAPACITY*2;
            String[] iterator1 = new String[DEFAULT_CAPACITY];
            int number = 0;
            for (String iter:this.iterator){
                iterator1[number] = iter;
                number++;
            }
            this.iterator = new String[DEFAULT_CAPACITY];
            for (number=0; number <size; number++){
                this.iterator[number] = iterator1[number];
            }
            this.iterator[size] = value;
            size++;
        }
    }
    public String get(int index) {
        if (index<size){
            return this.iterator[index];
        }else{
            return "Индекс больше величины массива";
        }
    }
    boolean isEmpty(){
        if (size<0)
            return true;
        else
            return false;
    }

    @Override
    public String toString() {
        return "Iterator{" +
                "size=" + size +
                ", iterator=" + Arrays.toString(iterator) +
                '}';
    }
}
