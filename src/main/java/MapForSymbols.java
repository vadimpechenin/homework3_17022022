import java.util.*;

public class MapForSymbols {
    private String [] strings;
    public TreeMap<String, Integer> map = new TreeMap<String, Integer>();
    public ArrayList<String> mapSorted = new ArrayList<String>();
    public MapForSymbols(String [] strings) {
        this.strings = strings;
        mapPut();
    }

    public void mapPut() {
        int number;
        for (String charSymbol : strings) {
            if (!charSymbol.equals(" ")) {
                if (map.containsKey(charSymbol)) {
                    number = map.get(charSymbol);
                    number += 1;
                    map.put(charSymbol, number);
                } else {
                    map.put(charSymbol, 1);
                }
            }
        }
        //Сортировкаю Записывам в листы
        ArrayList<String> keys = new ArrayList<String>();
        ArrayList<Integer> values = new ArrayList<Integer>();
        for (Map.Entry<String, Integer> entry : map.entrySet()) {
            // get key
            keys.add(entry.getKey());
            // get value
            values.add(entry.getValue());
        }

        boolean isSorted = false;
        int buf;
        int bufIndex;
        int[] indexes = new int[values.size()];
        for (int i = 0; i < values.size() - 1; i++){
                indexes[i] = i;
            }
        while(!isSorted) {
        isSorted = true;
        for (int i = 0; i < values.size()-1; i++) {
            if(values.get(i) < values.get(i+1)){
                isSorted = false;

                buf = values.get(i);
                values.set(i,values.get(i+1));
                values.set(i+1, buf);

                bufIndex = indexes[i];
                indexes[i] = indexes[i+1];
                indexes[i+1] = bufIndex;
                }
            }
        }
        System.out.println("Отсортированные в порядке убывания частоты символы");
        int k = 0;
        for (int i : indexes){
            this.mapSorted.add(keys.get(i) + ": " + String.valueOf(values.get(k)));
            System.out.println(this.mapSorted.get(k));
            k+=1;
        }
    }



}
