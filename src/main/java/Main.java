/*
Задания по теме Java Collections
Задание 1
• Реализовать Iterator<String> для обычного массива;
• Продемонстрировать работу получившегося Iterator;

Задание 2

Реализовать класс-конвертер перевода инфиксной записи в
постфиксную (обратная польская запись);
• В записи могут использоваться знаки: + - / * ()
• Продемонстрировать работу конвертера;

Алгоритм и пример:
https://ru.wikipedia.org/wiki/Обратная_польская_запись

Задание 3
• Реализовать класс, метод которого будет считать количество каждого
символа в строке и выводить в порядке убывания:
 Например для строки: «Как у тебя дела?» метод должен распечатать:
К: 2
Е: 2
А: 2
У: 1
Т: 1
Б: 1
…

 */


import java.util.Scanner;

public class Main {
    public static void main(String[] args){
        
    }
    static String iteratorTask1(){
        System.out.println(" ");
        System.out.println("Задача 1");
        System.out.println("Создание итератора. Реализован итератор String");
        Iterator iteratorObject = new Iterator();
        System.out.println("Добавление элементов - 10");
        for (int index = 1; index<11; index++) {
            iteratorObject.add("Слон " + String.valueOf(index));
        }
        System.out.println("Полученный массив");
        System.out.println(iteratorObject.toString());

        System.out.println("Получение 6 элемента");
        System.out.println(iteratorObject.get(5));
        return iteratorObject.get(5);
    }
    
    static String reversePolihNotation(){
        //Ввести 3 + 4 * 2 / ( 1 - 5 ) ^ 2
        /*Scanner in = new Scanner(System.in);
        String s = in.nextLine();*/
        System.out.println(" ");
        System.out.println("Задача 2");
        String s = "3 + 4 * 2 / ( 1 - 5 ) * 2";
        String [] strings = s.split(" ");
        Converter converter = new Converter(strings);
        String stringRPN = converter.convert();
        return stringRPN;
    }
    static String mapForSymbolsFunction(){
        //Ввести Как у тебя дела?
        /*Scanner in = new Scanner(System.in);
        String s = in.nextLine();*/
        System.out.println(" ");
        System.out.println("Задача 3");
        String s = "Как у тебя дела?";
        System.out.println(s);
        s = s.toUpperCase();
        String [] strings = s.split("");
        MapForSymbols resultMap = new MapForSymbols(strings);
        return resultMap.mapSorted.get(0);
    }

}
